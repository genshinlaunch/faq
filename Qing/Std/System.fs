module Qing.Std.System

open System
open System.Diagnostics

open Qing.Lang

let quit args _ =
    match args with
    | [ Int exitcode ] -> Environment.Exit(exitcode)
    | [] -> Environment.Exit(0)

    Nil

let print args _ =
    let s =
        match args with
        | [ String s ] -> s
        | [ obj ] -> obj.ToString()

    Misc.Out.WriteLine(s)
    Nil

let ask args _ =
    let prompt, passwordMode =
        match args with
        | [ String s; Bool b ] -> s, b
        | [ String s ] -> s, false

    printfn "%s" prompt

    if passwordMode then
        let rec loop input =
            let ck = Console.ReadKey(true)

            match ck.Key with
            | ConsoleKey.Enter ->
                printfn ""
                String input
            | ConsoleKey.Backspace ->
                match input with
                | "" -> ""
                | _ ->
                    printf "\b \b"
                    input[.. input.Length - 1]
                |> loop
            | _ ->
                printf "*"
                loop (input + ck.KeyChar.ToString())

        loop ""
    else
        match Console.ReadLine() with
        | null -> String ""
        | s -> String s

let gcCollect _ _ =
    GC.Collect()
    Nil

let clear _ _ =
    Console.Clear()
    Nil

let callCmd [ String cmd ] _ =
    use cmdProcess = new Process()
    cmdProcess.StartInfo.FileName <- "cmd.exe"
    cmdProcess.StartInfo.CreateNoWindow <- true
    cmdProcess.StartInfo.UseShellExecute <- false
    cmdProcess.StartInfo.RedirectStandardInput <- true
    cmdProcess.StartInfo.RedirectStandardOutput <- true
    cmdProcess.StartInfo.RedirectStandardError <- true

    cmdProcess.StartInfo.Arguments <- "/c " + cmd
    cmdProcess.Start() |> ignore

    let str = cmdProcess.StandardOutput.ReadToEnd()

    cmdProcess.WaitForExit()

    String str
