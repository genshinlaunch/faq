module Qing.Std.Collections

open Qing.Lang

let length args _ =
    match args with
    | [ String s ] -> s.Length
    | [ Binary b ] -> b.Length
    | [ Array a ] -> a.Count
    | [ Object o ] -> o.Count

let append args _ =
    match args with
    | [ String left; String right ] -> String(left + right)
    | Array left as result :: Array right :: ([ Bool true ] | []) ->
        for i in right do
            left.Add(i)

        result
    | [ Array arr as result; Array _ as obj; Bool false ]
    | [ Array arr as result; obj ] ->
        arr.Add(obj)
        result

let insert args _ =
    match args with
    | [ String left; String right; Int pos ] -> String(left[..pos] + right + right[pos..])
