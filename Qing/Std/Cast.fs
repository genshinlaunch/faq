module Qing.Std.Cast

open System
open System.Text

open Qing.Lang

let cast args _ =
    match args with
    | [ String "逻辑类型"; obj ] -> obj.ToBool() |> Bool
    | [ String "整数类型"; obj ] ->
        match obj with
        | Int _ as i -> i
        | Bool b -> (if b then 1 else 0) |> Int
        | Decimal d -> int d |> Int
        | String s -> int s |> Int
    | [ String "小数类型"; obj ] ->
        match obj with
        | Decimal _ as d -> d
        | Int i -> decimal i |> Decimal
        | String s -> decimal s |> Decimal
    | [ String "字符串类型"; obj ] ->
        match obj with
        | String _ as s -> s
        | _ -> obj.ToString() |> String

let decimalRound args _ =
    match args with
    | [ Decimal number; Int ndigits ] -> Math.Round(number, ndigits) |> Decimal

let private getEncoding args =
    match args with
    | [] -> Encoding.UTF8
    | [ String str ] ->
        try
            Encoding.GetEncoding(str)
        with :? ArgumentException ->
            Encoding.UTF8

let decodeStr args _ =
    match args with
    | Binary bytes :: args ->
        let encoding = getEncoding args

        try
            String(encoding.GetString(bytes))
        with :? ArgumentException as exn ->
            raise <| QingFuncException $"解码字符串函数调用失败=>{exn.Message}"

let encodeStr args _ =
    match args with
    | String str :: args ->
        let encoding = getEncoding args

        try
            Binary(encoding.GetBytes(str))
        with :? ArgumentException as exn ->
            raise <| QingFuncException $"编码字符串函数调用失败=>{exn.Message}"
