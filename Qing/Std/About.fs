module Qing.Std.About

open Qing.Lang

let about _ _ =
    Misc.Out.WriteLine(
        @"青语言由以下贡献者开发：
**柳州市柳江区数心开物软件工作室（原创开发），联系邮箱：qingyuyan@aliyun.com 。**
Faq(FshArpQing)由以下贡献者开发:
**宁波市镇海区源深琦东软件工作室开发, 联系邮箱zvmsbackend@outlook.com.**"
    )

    Nil


let version _ _ = String Misc.Version
