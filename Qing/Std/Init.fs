module Qing.Std.Init

open Qing.Lang

open About
open Cast
open Qing.Std.System

let loadNatives (context: Context) natives =
    for name, desc, impl in natives do
        context.SetFunc(name, Native.create name desc impl)

let initStd context =
    [ "关于", "无参，返回空；显示项目贡献者信息。", about
      "版本", "无参，返回字符串；显示当前版本信息。", version
      "转换", "参数1-字符串，参数2-任意，返回任意；第一个参数是转换的目标类型的字符串表示，第二个参数是要转换的数据", cast
      "保留小数", "参数1-小数，参数2-整数，返回小数；根据四舍五入设置小数位数，参数1为转换前的小数，参数2为要保留的小数位数", decimalRound
      "解码字符串", "参数1-二进制，可选参数2-字符串，返回字符串；根据第二个参数设置字符编码，默认为UTF8，对二进制数据解码为字符串", decodeStr
      "编码字符串", "参数1-字符串，可选参数2-字符串，返回二进制；根据第二个参数设置字符编码，默认为UTF8，将字符串编码为二进制", encodeStr
      "退出", "可选参数1-整数，返回空；退出程序，如果传入参数1，则作为程序的退出码", quit
      "显示", "参数1-任意，返回空；显示参数1的格式化字符串", print
      "获取输入", "参数1-字符串，可选参数2-逻辑，返回字符串；参数1是提示文本，会在控制台输出，然后获取用户输入，参数2为真是是密码模式", ask
      "垃圾回收", "无参，返回逻辑；主动调用一次垃圾回收", gcCollect
      "清屏", "无参，返回空；清空控制台上输出的内容", clear
      "调用命令", "参数1-字符串，返回字符串；调用cmd命令", callCmd ]
    |> loadNatives context
