module Qing.Program

open System
open System.IO
open System.Text

open Qing.Scripts
open Qing.Lang
open Qing.Std.Init

open Evaluator

let doMain (args: string[]) init =
    let libContext = Context()
    initStd libContext
    evals libContext (Parser.parseOrShowError "初始化脚本" Init.Script) |> ignore

    let userContext = Context(libContext)

    Console.InputEncoding <- UnicodeEncoding()

    match args with
    | [| fpath |] ->
        let fpath =
            if not <| fpath.StartsWith("/") && not <| fpath.Contains(':') then
                Directory.GetCurrentDirectory()
            else
                fpath

        if not <| File.Exists(fpath) then
            eprintfn "要执行的文件不存在"
        else
            let fi = FileInfo(fpath)
            let dir = fi.DirectoryName
            Directory.SetCurrentDirectory(dir)
            let source = File.ReadAllText(fpath)

            match Parser.tryParse fpath source with
            | Error(msg, pos) -> eprintfn "%O" msg
            | Ok ast -> evals userContext ast |> ignore
    | _ -> printfn "欢迎使用FshArpQing%s -- 数心开物 & 源深琦东工作室" Misc.Version

    let rec mainloop buffer =
        printf ">> "

        match Console.ReadLine() with
        | null -> 0
        | line ->
            let lineTrimmed = line.Trim()

            if lineTrimmed.EndsWith('~') then
                mainloop (buffer + lineTrimmed[.. lineTrimmed.Length - 1] + "\n")
            else
                let source = buffer + lineTrimmed

                try
                    match Parser.parseOrShowError "标准输入" source |> evals userContext with
                    | Nil -> ()
                    | result -> printfn "%O" result

                    mainloop ""
                with exn ->
                    eprintfn "%s" exn.Message
                    1

    mainloop ""


[<EntryPoint>]
let main args = doMain args initStd
