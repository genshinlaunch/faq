module Qing.Util

open System
open System.Collections.Generic
open System.Text

let joinStrings (separator: string) (strings: string seq) = String.Join(separator, strings)

let stringOrEllipsis visited value =
    if visited |> Set.contains value then "…" else string value

let stringToHexString (s: string) (encoding: Encoding) =
    let bytes: byte[] = encoding.GetBytes(s)
    joinStrings "" <| seq { for b in bytes -> Convert.ToString(b, 16) }

let hexStringToString (hs: string) (encoding: Encoding) =
    let chars = hs.Split([| '%' |], options = StringSplitOptions.RemoveEmptyEntries)
    [| for c in chars -> Convert.ToByte(c, 16) |] |> encoding.GetString

let hexStringToBytes (hexString: string) =
    let hexString = hexString.Replace(" ", "")
    [| for i in 0 .. hexString.Length / 2 -> Convert.ToByte(hexString[i * 2 .. i * 2 + 1], 16) |]

let bytesToHexString (bytes: byte[]) =
    match bytes with
    | null -> ""
    | _ -> joinStrings "" <| seq { for i in bytes -> i.ToString("X2") }

type Ordering =
    | Equal
    | Greater
    | Less
    | Unknown

    static member FromInt(i) =
        if i > 0 then Greater
        else if i = 0 then Equal
        else Less

let buildDict entries =
    let result = Dictionary()

    for key, value in entries do
        result[key] <- value

    result

let (|SCons|SEmpty|) seq =
    if Seq.isEmpty seq then
        SEmpty
    else
        SCons(Seq.head seq, Seq.tail seq)

let swap (x, y) = y, x

let takeLine (s: string) startby =
    String.Join("", s[startby..] |> Seq.takeWhile ((<>) '\n'))

let formatSourcePosition (source: string) pos =
    let rec loop lineStartPos line col i =
        if i = pos then
            lineStartPos, line, col
        else if source[i] = '\n' then
            loop (i + 1) (line + 1) 1 (i + 1)
        else
            loop lineStartPos line (col + 1) (i + 1)

    let lineStartPos, line, col = loop 0 1 1 0
    let lineSrc = takeLine source lineStartPos
    sprintf "%s\n%s^\n第%d行, 第%d列" lineSrc (String.replicate (col - 1) " ") line col
