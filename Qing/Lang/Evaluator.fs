module Qing.Lang.Evaluator

open Qing.Util

type ParamWithDefault =
    | VarWithDefault of name: string * def: QingValue
    | FuncWithDefault of name: string * def: IFunc

type Trace = Trace of filename: string * source: string * pos: int

exception QingException of message: string * trace: Trace list

exception QingBreak

exception QingContinue

exception QingReturn of value: QingValue

let inline private raiseFrom (ast: Ast<QingValue>) message =
    raise <| QingException(message, [ Trace(ast.FileName, ast.Source, ast.Pos) ])

let private (|ToDecimal|_|) =
    function
    | Int i -> Some <| decimal i
    | Decimal d -> Some d
    | _ -> None

let private (|ToDouble|_|) =
    function
    | Int i -> Some <| double i
    | Decimal d -> Some <| double d
    | _ -> None

let inline private arithOp left op right =
    match op with
    | Add -> left + right
    | Sub -> left - right
    | Mul -> left * right
    | Div -> left / right

let private invokeBinOp ast left op right =
    match left, op, right with
    | String left, Add, String right -> String(left + right)
    | String left, Add, _ -> String(left + string right)
    | _, Add, String right -> String(string left + right)
    | String left, Sub, String right ->
        let lengthdiff = left.Length - right.Length

        let rec loop i =
            if i > lengthdiff then
                String left
            else if left[i .. i + right.Length] = right then
                String(left[..i] + left[i + right.Length ..])
            else
                loop (i + 1)

        loop 0
    | String str, Mul, Int multiplier -> str |> Seq.replicate multiplier |> joinStrings "" |> String
    | String str, Div, Int divisor -> String str[.. str.Length / divisor]
    | Int left, Mod, Int right -> Int(left % right)
    | _, Div, (Int 0 | Decimal 0M) -> raiseFrom ast "除数不能为零"
    | Int left, Pow, Int right -> Int(pown left right)
    | ToDouble left, Pow, ToDouble right -> Decimal(System.Math.Pow(left, right) |> decimal)
    | Int left, Div, Int right -> Decimal(decimal left / decimal right)
    | Int left, (Add | Sub | Mul), Int right -> arithOp left op right |> Int
    | ToDecimal left, (Add | Sub | Mul | Div), ToDecimal right -> arithOp left op right |> Decimal
    | _, Eq, _ -> Bool(left.CompareTo(right) = Equal)
    | _, Ne, _ -> Bool(left.CompareTo(right) <> Equal)
    | _, Lt, _ -> Bool(left.CompareTo(right) = Less)
    | _, Gt, _ -> Bool(left.CompareTo(right) = Greater)
    | _, Le, _ ->
        match left.CompareTo(right) with
        | Less
        | Equal -> Bool true
        | _ -> Bool false
    | _, Ge, _ ->
        match left.CompareTo(right) with
        | Greater
        | Equal -> Bool true
        | _ -> Bool false
    | _ -> raiseFrom ast $"{op}运算参数错误"

[<CompiledName("EvalExpr")>]
let rec eval (context: Context) ast : QingValue =
    match ast.Data with
    | Constant c -> c
    | Not x -> (eval context x).ToBool() |> not |> Bool
    | BinOp(left, op, right) ->
        match op with
        | And ->
            let left = eval context left
            if not <| left.ToBool() then left else eval context right
        | Or ->
            let left = eval context left
            if left.ToBool() then left else eval context right
        | _ -> invokeBinOp ast (eval context left) op (eval context right)
    | AstData.Path path -> evalPath context ast path
    | AstData.Array elts -> Array(ResizeArray(Seq.map (eval context) elts))
    | AstData.Object entries ->
        let result = QingObject()

        for key, value in entries do
            let value = eval context value

            match key with
            | Var name ->
                match value with
                | Func _ -> raiseFrom ast $"不可用函数赋值"
                | _ -> result.SetVar(name, value)
            | VarOrFunc.Func name ->
                match value with
                | Func fn -> result.SetFunc(name, fn)
                | _ -> raiseFrom ast $"{value.GetTypeString()}不是函数"

        Object result
    | AstData.Tag(name, attrs, children) ->
        let attrs = seq { for key, value in attrs -> key, eval context value } |> buildDict

        { Name = name
          Attrs = attrs
          Children = evalOption context children }
        |> Tag
    | Lambda(async, parameters, defparams, desc, body) ->
        let defParams =
            [ for param, def in defparams ->
                  match param with
                  | Var name ->
                      match eval context def with
                      | Func _ -> raiseFrom ast $"不能以函数为默认值"
                      | def -> VarWithDefault(name, def)
                  | VarOrFunc.Func name ->
                      match eval context def with
                      | Func fn -> FuncWithDefault(name, fn)
                      | def -> raiseFrom ast $"{def.GetTypeString()}不是函数" ]

        makeQingFunc parameters defParams body async context (desc |> Option.defaultValue "")
        |> Func
    | If(test, body, orelse) ->
        if (eval context test).ToBool() then
            evals context body
        else
            evals context orelse
    | Assign(recursive, target, value) ->
        let value = eval context value
        evalAssign recursive context ast target value
        value
    | AugAssign(target, op, value) ->
        let left = evalPath context ast target
        let value = invokeBinOp ast left op (eval context value)
        evalAssign false context ast target value
        value
    | For(init, test, step, body) ->
        let loopCtx = Context(context)
        eval loopCtx init |> ignore

        let rec loop i =
            if (eval loopCtx test).ToBool() then
                try
                    evals loopCtx body |> ignore
                    eval loopCtx step |> ignore
                    loop (i + 1)
                with
                | QingBreak -> Int i
                | QingContinue ->
                    eval loopCtx step |> ignore
                    loop (i + 1)
            else
                Int i

        loop 0
    | While(test, body) -> evalWhile context (Context(context)) test body
    | Until(test, body) ->
        let loopCtx = Context(context)
        evals context body |> ignore
        evalWhile context loopCtx test body
    | Repeat(times, body) ->
        let loopCtx = Context(context)

        match eval context times with
        | Int times when times > 0 ->
            let rec loop i times =
                if times = 0 then
                    Int i
                else
                    try
                        evals loopCtx body |> ignore
                        loop (i + 1) (times - 1)
                    with
                    | QingBreak -> Int i
                    | QingContinue -> loop (i + 1) times

            loop 0 times
        | times' -> raiseFrom times $"{times'.GetTypeString()}不是正整数"
    | ForEach(target, iter, body) ->
        let loopCtx = Context(context)

        let seq: QingValue seq =
            match eval context iter with
            | Array array -> array
            | Object object -> seq { for key, value in object -> Array(ResizeArray([ String key; value ])) }
            | String str -> Seq.map (string >> String) str
            | seq -> raiseFrom iter $"{seq.GetTypeString()}不可迭代"

        let rec loop i seq =
            match seq with
            | SEmpty -> Int i
            | SCons(x, xs) ->
                match context.Set(false, target, x) with
                | Ok() -> ()
                | Error msg -> raiseFrom ast msg

                try
                    evals loopCtx body |> ignore
                    loop (i + 1) xs
                with
                | QingBreak -> Int i
                | QingContinue -> loop (i + 1) xs

        loop 0 seq
    | Try(body, Handler(target, handlerbody), finalbody) ->
        let tryCtx = Context(context)

        try
            evals tryCtx body |> ignore
        with QingException(message, trace) as exn ->
            let Trace(filename, _, pos) :: _ = List.rev trace

            let excObj =
                QingObject([ "信息", String message; "位置", Int pos; "来源", String filename ])

            let handlerCtx = Context(context)
            handlerCtx.SetVar(false, target, Object excObj)
            evals handlerCtx handlerbody |> ignore

        let finalCtx = Context(context)
        evals finalCtx finalbody |> ignore

        Nil
    | Throw exn ->
        match eval context exn with
        | String message -> raiseFrom ast message
        | exn -> raiseFrom ast (string exn)
    | Return value -> value |> eval context |> QingReturn |> raise
    | Break -> raise QingBreak
    | Continue -> raise QingContinue
    | Switch(subject, cases, def) ->
        let subject = eval context subject

        let rec loop cases =
            match cases with
            | [] -> evals context def
            | Case(value, body) :: cases ->
                match subject.CompareTo(eval context value) with
                | Equal -> evals context body
                | _ -> loop cases

        loop cases
    | Await task ->
        match eval context task with
        | Task task ->
            task.Wait()
            task.Result
        | value -> raiseFrom ast $"{value.GetTypeString()}不是任务"

and stringToVarOrFunc ast =
    function
    | "" -> raiseFrom ast "索引不能为空"
    | s ->
        match s[0] with
        | '#' -> Var s[1..]
        | '@' -> VarOrFunc.Func s[1..]
        | _ -> raiseFrom ast $"无效的索引\"{s}\""

and private evalAssign recursive context ast target value =
    match target with
    | Path(root, []) ->
        match context.Set(recursive, root, value) with
        | Ok() -> ()
        | Error msg -> raiseFrom ast msg
    | Path(root, segments) ->
        let last :: init = List.rev segments
        let path = Path(root, List.rev init)
        let subject = evalPath context ast path

        let setIntIndex index =
            match subject with
            | Array array -> array[index] <- value
            | _ -> raiseFrom ast $"{subject.GetTypeString()}不可用作索引"

        match last with
        | VarSegment name ->
            match subject with
            | Object object ->
                match value with
                | Func _ -> raiseFrom ast "不可用函数赋值"
                | _ -> object.SetVar(name, value)
            | _ ->
                match context.GetVar(name) with
                | Some(Int index) -> setIntIndex index
                | Some index -> raiseFrom ast $"{index.GetTypeString()}不可用作索引"
                | None -> raiseFrom ast $"变量\"#{name}\"未定义"
        | FuncSegment name ->
            match subject, value with
            | Object object, Func fn -> object.SetFunc(name, fn)
            | Object _, _ -> raiseFrom ast $"{value.GetTypeString()}不是函数"
            | _ -> raiseFrom ast $"{subject.GetTypeString()}没有属性"
        | IntSegment index -> setIntIndex index
        | ExprSegment expr ->
            match eval context expr with
            | Int index -> setIntIndex index
            | String name ->
                match subject with
                | Object object ->
                    match object.Set(false, stringToVarOrFunc ast name, value) with
                    | Ok() -> ()
                    | Error msg -> raiseFrom ast msg
                | _ -> raiseFrom ast $"{subject.GetTypeString()}没有属性"
            | value -> raiseFrom ast $"{value.GetTypeString()}不可用作索引"

and private evalPath context ast (Path(root, segments)) =
    List.fold (foldSegments context ast) (evalPathRoot context ast root) segments

and private evalPathRoot context ast root =
    match root with
    | Var name -> context.GetVar(name) |> Option.defaultValue Nil
    | VarOrFunc.Func name ->
        match context.GetFunc(name) with
        | Some fn -> Func fn
        | None -> raiseFrom ast $"函数\"@{name}\"不存在"

and private foldSegments context ast acc segment =
    let intIndex index =
        match acc with
        | Array array ->
            if index >= array.Count || index < 0 then
                raiseFrom ast "数组下标越界"
            else
                array[index]
        | String str ->
            if index >= str.Length || index < 0 then
                raiseFrom ast "字符串下标越界"
            else
                str[index] |> string |> String

    match segment with
    | VarSegment name ->
        match acc with
        | Object object ->
            match object.GetVar(name) with
            | Some value -> value
            | None -> raiseFrom ast $"\"#{name}\"不在对象内"
        | _ ->
            match context.GetVar(name) with
            | Some(Int index) -> intIndex index
            | Some index -> raiseFrom ast $"{index.GetTypeString()}不可用作索引"
            | None -> raiseFrom ast $"变量\"#{name}\"不存在"
    | FuncSegment name ->
        match acc with
        | Object object ->
            match object.GetFunc(name) with
            | Some fn -> fn.Bind(object) |> Func
            | None -> raiseFrom ast $"\"@{name}\"不在对象内"
        | _ -> raiseFrom ast $"{acc.GetTypeString()}里没有函数"
    | IntSegment index -> intIndex index
    | ExprSegment expr ->
        match eval context expr with
        | Int index -> intIndex index
        | String key ->
            match acc with
            | Object object ->
                match stringToVarOrFunc ast key with
                | Var name ->
                    match object.GetVar(name) with
                    | Some value -> value
                    | None -> raiseFrom ast $"\"{key}\"不在对象内"
                | VarOrFunc.Func name ->
                    match object.GetFunc(name) with
                    | Some fn -> fn.Bind(object) |> Func
                    | None -> raiseFrom ast $"\"{key}\"不在对象内"
            | _ -> raiseFrom ast $"{acc.GetTypeString()}不可索引"
        | value -> raiseFrom ast $"{value.GetTypeString()}不可用作索引"
    | CallSegment(posargs, kwargs) ->
        match acc with
        | Func func ->
            try
                let posargs = List.map (eval context) posargs
                let kwargs = seq { for key, value in kwargs -> key, eval context value } |> Map
                func.Invoke(posargs, kwargs, context)
            with
            | QingReturn value -> value
            | QingException(message, trace) ->
                raise
                <| QingException(message, Trace(ast.FileName, ast.Source, ast.Pos) :: trace)
            | QingFuncException message -> raiseFrom ast message
            | QingBreak
            | QingContinue -> raiseFrom ast "循环外的跳出/继续"
        | _ -> raiseFrom ast $"{acc.GetTypeString()}不可调用"

and [<CompiledName("EvalList")>] evals context asts =
    let rec loop asts =
        match asts with
        | [] -> Nil
        | [ ast ] -> eval context ast
        | ast :: asts ->
            eval context ast |> ignore
            loop asts

    loop asts

and private evalOption context maybeAst =
    match maybeAst with
    | Some ast -> eval context ast
    | None -> Nil

and private evalWhile context loopCtx test body =
    let rec loop i =
        if (eval context test).ToBool() then
            try
                evals loopCtx body |> ignore
                loop (i + 1)
            with
            | QingBreak -> Int i
            | QingContinue -> loop (i + 1)
        else
            Int i

    loop 0

and private loadArgs parameters defParams posargs kwargs (ctx: Context) =
    let rec loop parameters defParams posargs =
        match parameters, defParams, posargs with
        | [], [], _ -> ()
        | [], VarWithDefault(name, _) :: defParams, (posarg :: posargs' as posargs) ->
            let value, posargs =
                match Map.tryFind (Var name) kwargs with
                | Some value -> value, posargs
                | None -> posarg, posargs'

            match value with
            | Func _ -> raise <| QingFuncException $"函数不可用作\"#{name}\"的值"
            | _ ->
                ctx.SetVar(name, value)
                loop [] defParams posargs
        | [], FuncWithDefault(name, _) :: defParams, (posarg :: posargs' as posargs) ->
            let fn, posargs =
                match Map.tryFind (VarOrFunc.Func name) kwargs with
                | Some fn -> fn, posargs
                | None -> posarg, posargs'

            match fn with
            | Func fn ->
                ctx.SetFunc(name, fn)
                loop [] defParams posargs
            | _ -> raise <| QingFuncException $"\"@{name}\"的值必须为函数"
        | [], VarWithDefault(name, def) :: defParams, [] ->
            match Map.tryFind (Var name) kwargs with
            | Some value -> ctx.SetVar(name, value)
            | None -> ctx.SetVar(name, def)

            loop [] defParams []
        | [], FuncWithDefault(name, def) :: defParams, [] ->
            match Map.tryFind (VarOrFunc.Func name) kwargs with
            | Some(Func fn) -> ctx.SetFunc(name, fn)
            | Some value -> raise <| QingFuncException $"{value.GetTypeString()}不是函数"
            | None -> ctx.SetFunc(name, def)

            loop [] defParams []
        | target :: parameters, defParams, posargs ->
            match Map.tryFind target kwargs with
            | Some value ->
                match ctx.Set(false, target, value) with
                | Ok() -> ()
                | Error msg -> raise <| QingFuncException msg
            | None ->
                match posargs with
                | posarg :: posargs ->
                    match ctx.Set(false, target, posarg) with
                    | Ok() -> ()
                    | Error msg -> raise <| QingFuncException msg

                    loop parameters defParams posargs
                | [] ->
                    match target with
                    | Var name -> ctx.SetVar(name, Nil)
                    | VarOrFunc.Func name -> raise <| QingFuncException $"没有提供与\"@{name}\"对应的参数"

                    loop parameters defParams []

    loop parameters defParams posargs

and private makeQingFunc parameters defParams code async closure desc =
    { new IFunc with
        member _.ToString() =
            let paramsString =
                seq {
                    for param in parameters -> string param

                    for defparam in defParams ->
                        match defparam with
                        | VarWithDefault(name, def) -> $"#{name}={def}"
                        | FuncWithDefault(name, def) -> $"@{name}={def}"
                }
                |> joinStrings "，"

            sprintf "%s@【%s】｛%s｝" (if async then "异步" else "") paramsString desc

        member _.Invoke(posargs, kwargs, _) =
            let ctx = Context(closure)

            loadArgs parameters defParams posargs kwargs ctx

            if async then
                let task = task { return evals ctx code }
                task.Start()
                Task task
            else
                evals ctx code

        member this.Bind(_) = this }
