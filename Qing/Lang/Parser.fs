module Qing.Lang.Parser

open System.Globalization

open Qing.Util

open Parsec

exception QingParserException of message: string * pos: int

let withsourceinfo parser (stream: CharStream) =
    let pos = stream.Pos

    match parser stream with
    | Ok result ->
        Ok
            { Data = result
              Pos = pos
              FileName = stream.FileName
              Source = stream.Source }
    | Error msg -> Error msg

let private spaces = regex @"[\s,，]*([;；]{2}[\s\S]*[;；]{2}|[;；].*|[\s,，]*)*" >>% ()

let private spaces' (stream: CharStream) =
    if stream.Satisfy(fun c -> System.Char.IsLetterOrDigit(c) || c = '_') then
        fails "空白" stream
    else
        spaces stream

module private Tokens =
    let lquotec = pstring "“"
    let lquotee = pstring "\""
    let rquotec = pstring "”"
    let rquotee = pstring "\""
    let token re = regex re >>. spaces >>% ()
    let tokenSpaces s = pstring s >>. spaces' >>% ()
    let lbracket = token @"\[|【"
    let rbracket = token @"\]|】"
    let lbrace = token @"\{|｛"
    let rbrace = token @"\}|｝"
    let lparen = token @"\(|（"
    let rparen = token @"\)|）"
    let ltag = token "《"
    let rtag = token "》"
    let at = token "@"
    let dot = token "、"

    let operator eng chn result =
        pstring eng >>. spaces <|> (regex chn >>. spaces') >>% result

    let assign = regex ":|：" >>. spaces <|> (pstring "设为" >>. spaces') >>% ()
    let assignrec = operator "=" "为" ()
    let addeq = operator "+=" "自加|加等" Add
    let subeq = operator "-=" "自减|减等" Sub
    let muleq = operator "*=" "自乘|乘等" Mul
    let diveq = operator "/=" "自除|除等" Div
    let modeq = operator "%=" "自模|模等" Mod
    let poweq = pstring "**=" >>. spaces >>% Pow
    let add = operator "+" "加" Add
    let sub = operator "-" "减" Sub
    let mul = operator "*" "乘" Mul
    let div = operator "/" "除" Div
    let mod' = operator "%" "模" Mod
    let pow = pstring "**" >>. spaces >>% Pow
    let eq = operator "==" "等于" Eq

    let ne =
        ([ "!="; "<>" ] |> List.map pstring |> choice >>. spaces)
        <|> (pstring "不等于" >>. spaces)
        >>% Ne

    let lt = operator "<" "小于" Lt
    let gt = operator ">" "大于" Gt
    let le = operator "<=" "小于等于" Le
    let ge = operator ">=" "大于等于" Ge
    let and' = operator "&&" "且" And
    let or' = operator "||" "或" Or
    let not' = tokenSpaces "取反"
    let async = tokenSpaces "异步"
    let if' = tokenSpaces "如果"
    let elif' = tokenSpaces "再则"
    let else' = tokenSpaces "否则"
    let while' = tokenSpaces "当"
    let do' = tokenSpaces "执行"
    let until = tokenSpaces "直到"
    let repeat = tokenSpaces "重复"
    let foreach = tokenSpaces "遍历"
    let in' = tokenSpaces "为"
    let try' = tokenSpaces "尝试"
    let catch = tokenSpaces "排查"
    let finally' = tokenSpaces "例行"
    let throw = tokenSpaces "抛出"
    let return' = tokenSpaces "返回"
    let break' = tokenSpaces "跳出"
    let continue' = tokenSpaces "继续"
    let switch = tokenSpaces "匹配"
    let default' = tokenSpaces "默认"
    let await = tokenSpaces "等待"

open Tokens

module Parsers =
    let past, pastRef = createForwardReference ()

    let pblock = lbrace >>. many past .>> rbrace .>> spaces

    let quotedstring =
        let unescapedChar = noneOf "\\\"”"

        let escapedChar =
            [ ("\\\"", '"') //"
              ("\\”", '”')
              ("\\\\", '\\')
              ("\\/", '/')
              ("\\b", '\b')
              ("\\f", '\f')
              ("\\n", '\n')
              ("\\r", '\r')
              ("\\t", '\t') ]
            |> List.map (fun (toMatch, result) -> pstring toMatch >>% result)
            |> choice

        let unicodeChar =
            parse {
                let! m = regex @"\\u(\d{4})"
                return System.Int32.Parse(m.Groups[1].Value, NumberStyles.HexNumber) |> char
            }

        let onechar = choice [ unescapedChar; escapedChar; unicodeChar ]

        manyChars onechar |> between lquotec rquotec
        <|> (manyChars onechar |> between lquotee rquotee)
        .>> spaces

    let pconstant =
        let pint =
            parse {
                let! s = regexs @"[+-]?\d+" .>> spaces'

                match System.Int32.TryParse(s) with
                | true, i -> return Int i
                | false, _ -> return! fails "整数字面量错误"
            }

        let pdecimal =
            parse {
                let! s = regexs @"[+-]?\d+\.\d+" .>> spaces'

                match System.Decimal.TryParse(s) with
                | true, d -> return Decimal d
                | false, _ -> return! fails "小数字面量错误"
            }

        let pbool = (pstring "真" >>% Bool true) <|> (pstring "假" >>% Bool false) .>> spaces'
        let pnil = pstring "空" .>> spaces' >>% Nil

        let pbinary =
            regex @"0[xX]([\da-fA-F]+)" .>> spaces'
            |>> (group 1 >> hexStringToBytes >> Binary)

        let pstring = quotedstring |>> String

        let pquote = pblock |>> Quote

        [ pdecimal; pbinary; pint; pbool; pnil; pstring; pquote ]
        |> choice
        |>> Constant

    let pvar' = regex @"#(\w+)" |>> group 1
    let pfunc' = regex @"@(\w+)" |>> group 1

    let pvar = pvar' |>> Var
    let pfunc = pfunc' |>> VarOrFunc.Func
    let pvarorfunc = pvar <|> pfunc

    let ppath =
        let pseg =
            let varOrInt (s: string) =
                match System.Int32.TryParse(s) with
                | true, i -> IntSegment i
                | false, _ -> VarSegment s

            let pargs =
                let rec loop posargs kwargs =
                    parse {
                        match! opt (pvarorfunc .>> spaces .>> assignrec) with
                        | Some key when kwargs |> Seq.map fst |> Seq.contains key -> return! fails "重复的关键字参数"
                        | Some key ->
                            let! value = past
                            return! loop posargs ((key, value) :: kwargs)
                        | None ->
                            match! opt past with
                            | Some _ when kwargs |> List.isEmpty |> not -> return! fails "位置参数必须在关键字参数之后"
                            | Some posarg -> return! loop (posarg :: posargs) kwargs
                            | None ->
                                return CallSegment(List.rev posargs, List.rev kwargs)
                    }

                loop [] []

            [ pvar' |>> varOrInt
              pfunc' |>> FuncSegment
              pstring "#" >>. lbracket >>. past .>> (regex @"\]|］") |>> ExprSegment
              lbracket >>. pargs .>> (regex @"\]|］") ]
            |> choice

        let psegs =
            many pseg .>>. opt (dot >>. past)
            |>> (function
            | segs, None -> segs
            | init, Some last -> init @ [ CallSegment([ last ], []) ])

        pvarorfunc .>>. psegs .>> spaces' |>> Path

    let parray = lbracket >>. spaces >>. many past .>> rbracket |>> AstData.Array

    let pobjectorlambda =
        let rec lambdalist parameters defparams =
            parse {
                match! opt (pvarorfunc .>> spaces) with
                | Some key when defparams |> Seq.map fst |> Seq.append parameters |> Seq.contains key ->
                    return! fails "重复的参数"
                | Some key ->
                    match! opt (assignrec >>. past) with
                    | Some def -> return! lambdalist parameters ((key, def) :: defparams)
                    | None when defparams |> List.isEmpty |> not -> return! fails "位置参数必须在关键字参数之后"
                    | None -> return! lambdalist (key :: parameters) defparams
                | None ->
                    let! desc = opt quotedstring
                    return List.rev parameters, List.rev defparams, desc
            }
        let rec objectentries entries =
            parse {
                match! opt (pvarorfunc .>> spaces .>> assign) with
                | Some key when entries |> Seq.map fst |> Seq.contains key -> return! fails "重复的对象键"
                | Some key ->
                    let! value = past
                    return! objectentries ((key, value) :: entries)
                | None -> return AstData.Object(List.rev entries)
            }

        parse {
            do! at
            match! opt lbrace with
            | Some() ->
                return! objectentries [] .>> spaces .>> rbrace
            | None ->
                let! async = opt async |>> (fun o -> o.IsSome)
                let! parameters, defparams, desc = lambdalist [] []
                let! body = rbracket >>. pblock
                return Lambda(async, parameters, defparams, desc, body)
        }

    let ptag =
        let rec loop acc =
            parse {
                match! opt (pvar' .>> spaces .>> assign) with
                | Some key when acc |> Seq.map fst |> Seq.contains key -> return! fails "重复的标签属性"
                | Some key ->
                    let! value = past
                    return! loop ((key, value) :: acc)
                | None ->
                    let! children = opt (spaces >>. past)
                    return List.rev acc, children
            }

        ltag >>. regexs @"\w+" .>>. loop [] .>> rtag
        |>> (fun (name, (attrs, children)) -> AstData.Tag(name, attrs, children))

    let pparen = lparen >>. past .>> rparen

    let ptarget =
        parse {
            match! ppath with
            | Path(_, []) as target -> return target
            | Path(_, subsequence) when
                (match List.last subsequence with
                 | CallSegment _ -> true
                 | _ -> false)
                ->
                return! fails "不能对函数调用赋值"
            | target -> return target
        }

    let passign =
        let augops = [ addeq; subeq; muleq; diveq; modeq; poweq ] |> choice

        parse {
            let! target = ptarget
            match! opt augops with
            | Some augop ->
                let! value = past
                return AugAssign(target, augop, value)
            | None ->
                let! recursive = assign >>% false <|> (assignrec >>% true)
                let! value = past
                return Assign(recursive, target, value)
        }
        |> attempt

    let pif =
        parse {
            let! test = if' >>. past
            let! body = pblock

            let! filename = askfilename
            let! source = asksource

            let! elifs =
                many
                <| parse {
                    let! pos = askpos
                    let! test = elif' >>. past
                    let! body = pblock
                    return pos, test, body
                }

            let! orelse = opt (else' >>. pblock) |>> Option.defaultValue []

            let orelse =
                let folder orelse (pos, test, body) =
                    [ { Data = If(test, body, orelse)
                        Pos = pos
                        FileName = filename
                        Source = source } ]

                List.fold folder orelse elifs

            return If(test, body, orelse)
        }

    let pfororwhile =
        parse {
            let! testorinit = while' >>. past

            match! opt (past .>>. past) with
            | Some(test, step) ->
                let init = testorinit
                let! body = pblock
                return For(test, init, step, body)
            | None ->
                let test = testorinit
                let! body = pblock
                return While(test, body)
        }

    let puntil = do' >>. pblock .>> until .>>. past |>> (swap >> Until)

    let prepeat = repeat >>. past .>>. pblock |>> Repeat

    let pforeach =
        foreach >>. past .>> in' .>>. pvarorfunc .>> spaces .>>. pblock
        |>> (fun ((iter, target), body) -> ForEach(target, iter, body))

    let ptry =
        let f (((body, target), handlerbody), finalbody) =
            Try(body, Handler(target, handlerbody), finalbody |> Option.defaultValue [])

        try' >>. pblock .>> catch .>>. pvar' .>> spaces
        .>>. pblock
        .>>. opt (finally' >>. pblock)
        |>> f

    let pthrow = throw >>. past |>> Throw
    let preturn' = return' >>. past |>> Return
    let pbreak = break' >>% Break
    let pcontinue = continue' >>% Continue
    let pawait = await >>. past |>> Await

    let pswitch =
        let rec loop cases =
            parse {
                match! opt past with
                | Some value ->
                    let! body = pblock
                    return! loop (Case(value, body) :: cases)
                | None ->
                    let! def = opt (default' >>. pblock)
                    return List.rev cases, def |> Option.defaultValue []
            }

        parse {
            let! subject = switch >>. past .>> rbrace
            let! cases, def = loop [] .>> rbrace
            return Switch(subject, cases, def)
        }

    let patom =
        [ ppath |>> AstData.Path; pconstant ]
        |> choice
        |> withsourceinfo
        <|> pparen

    let pbinop =
        let pfactor = patom <|> (not' >>. patom |>> Not |> withsourceinfo)

        let binop operand ops =
            let maybeOpAndRight = choice ops .>>. operand |> opt

            let rec loop acc =
                parse {
                    let! filename = askfilename
                    let! source = asksource
                    let! pos = askpos

                    match! maybeOpAndRight with
                    | Some(op, right) ->
                        return!
                            loop
                                { Data = BinOp(acc, op, right)
                                  FileName = filename
                                  Source = source
                                  Pos = pos }
                    | None -> return acc
                }

            operand >>= loop

        let ppower = binop pfactor [ pow ]
        let pterm = binop ppower [ mul; div; mod' ]
        let parith = binop pterm [ add; sub ]
        let pcomp = binop parith [ eq; ne; lt; gt; le; ge ]
        let pand = binop pcomp [ and' ]
        
        binop pand [ or' ]

    pastRef.Value <-
        [ passign
          pobjectorlambda
          parray
          ptag
          pif
          pfororwhile
          puntil
          prepeat
          pforeach
          ptry
          pthrow
          preturn'
          pbreak
          pcontinue
          pswitch
          pawait  ]
        |> choice
        |> withsourceinfo
        <|> pbinop

    let pmodule =
        let rec loop acc =
            parse {
                match! opt eof with
                | Some() -> return List.rev acc
                | None ->
                    let! ast = past
                    return! loop (ast :: acc)
            }
        spaces >>. loop []

let tryParse filename source =
    let stream = CharStream(filename, source)

    match Parsers.pmodule <| stream with
    | Ok result -> Ok result
    | Error msg -> Error(msg, stream.Pos)

let tryParseFromStdin source = tryParse "标准输入" source

let parse filename source =
    match tryParse filename source with
    | Ok result -> result
    | Error(msg, pos) -> raise <| QingParserException(msg, pos)

let parseFromStdin source = parse "标准输入" source

let parseOrShowError filename source =
    match tryParse filename source with
    | Ok result -> result
    | Error(msg, pos) ->
        sprintf "编译错误: %s\n%s" msg (formatSourcePosition source pos)
        |> Misc.Error.WriteLine

        []
