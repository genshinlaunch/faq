module Qing.Lang.Misc

open System
open System.IO

[<Literal>]
let Version = "1.0"

let mutable Out = Console.Out

let mutable Error = Console.Error
