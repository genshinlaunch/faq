namespace Qing.Lang

open System
open System.Collections.Generic
open System.Threading.Tasks

open Qing.Util

exception QingFuncException of message: string

#nowarn "0025"

type QingValue =
    | Nil
    | Binary of byte[]
    | Bool of bool
    | Int of int
    | Decimal of decimal
    | String of string
    | Object of QingObject
    | Array of ResizeArray<QingValue>
    | Tag of Tag
    | Func of IFunc
    | Task of Task<QingValue>
    | Quote of Ast<QingValue> list

    // C#程序员用联合不方便, 所以提供像Option.IsSome, Option.Value这样的方法.
    // 不支持代数数据类型的语言都是**语言(确信).
    // 本来这些属性写得更紧凑一些, 但Fantomas加了空行与换行.

    // 另外F#会给联合用例生成Is...的内部属性供模式匹配使用, 所以我给每个属性都加了冠词.
    member this.IsANil =
        match this with
        | Nil -> true
        | _ -> false

    member this.IsABinary =
        match this with
        | Binary _ -> true
        | _ -> false

    member this.IsABool =
        match this with
        | Bool _ -> true
        | _ -> false

    member this.IsAnInt =
        match this with
        | Int _ -> true
        | _ -> false

    member this.IsADecimal =
        match this with
        | Decimal _ -> true
        | _ -> false

    member this.IsAString =
        match this with
        | String _ -> true
        | _ -> false

    member this.IsAnObject =
        match this with
        | Object _ -> true
        | _ -> false

    member this.IsAnArray =
        match this with
        | Array _ -> true
        | _ -> false

    member this.IsATag =
        match this with
        | Tag _ -> true
        | _ -> false

    member this.IsAFunc =
        match this with
        | Func _ -> true
        | _ -> false

    member this.IsATask =
        match this with
        | Task _ -> true
        | _ -> false

    member this.IsAQuote =
        match this with
        | Quote _ -> true
        | _ -> false

    member this.BinaryValue =
        match this with
        | Binary binary -> binary

    member this.BoolValue =
        match this with
        | Bool bool -> bool

    member this.IntValue =
        match this with
        | Int int -> int

    member this.DecimalValue =
        match this with
        | Decimal decimal -> decimal

    member this.StringValue =
        match this with
        | String string -> string

    member this.ObjectValue =
        match this with
        | Object object -> object

    member this.ArrayValue =
        match this with
        | Array array -> array

    member this.TagValue =
        match this with
        | Tag tag -> tag

    member this.FuncValue =
        match this with
        | Func func -> func

    member this.TaskValue =
        match this with
        | Task task -> task

    member this.QuoteValue =
        match this with
        | Quote quote -> quote

    // 用于ToStringAux里的Set.
    interface IComparable with
        member this.CompareTo(that) =
            this.GetHashCode().CompareTo(that.GetHashCode())

    interface IComparable<QingValue> with
        member this.CompareTo(that) =
            this.GetHashCode().CompareTo(that.GetHashCode())

    // 真的CompareTo.
    member this.CompareTo(that) =
        match this, that with
        | Nil, Nil -> Equal
        | Int left, Int right -> Ordering.FromInt(left.CompareTo(right))
        | Int left, Decimal right -> Ordering.FromInt((decimal left).CompareTo(right))
        | Decimal left, Int right -> Ordering.FromInt(left.CompareTo(decimal right))
        | Decimal left, Decimal right -> Ordering.FromInt(left.CompareTo(right))
        | Array left, Array right ->
            if left.Count <> right.Count then
                Unknown
            else if left |> Seq.zip right |> Seq.forall (fun (x, y) -> x.CompareTo(y) = Equal) then
                Equal
            else
                Unknown
        | Bool true, Bool true -> Equal
        | Bool false, Bool false -> Equal
        | Binary left, Binary right ->
            // 为什么数组支持比较运算符却不实现IComparable?
            if left > right then Greater
            else if left = right then Equal
            else Less
        | String left, String right -> Ordering.FromInt(left.CompareTo(right))
        | _ -> Unknown

    member this.GetTypeString() =
        match this with
        | Nil -> "空类型"
        | Binary _ -> "二进制类型"
        | Bool _ -> "逻辑类型"
        | Int _ -> "整数类型"
        | Decimal _ -> "小数类型"
        | String _ -> "字符串类型"
        | Array _ -> "数组类型"
        | Object _ -> "对象类型"
        | Task _ -> "任务类型"
        | Quote _ -> "代码引用类型"
        | Tag _ -> "标签类型"
        | Func _ -> "函数类型"

    member this.ToBool() =
        match this with
        | Nil
        | Bool false
        | String ""
        | Binary [||] -> false
        | Array array when array.Count = 0 -> false
        | Object object when object.Count = 0 -> false
        | _ -> true

    member private this.ToStringAux(visited) =
        match this with
        | Nil -> "空"
        | Bool true -> "真"
        | Bool false -> "假"
        | Binary binary -> bytesToHexString binary
        | Int int -> string int
        | Decimal decimal ->
            let fstr = string decimal
            if fstr.Contains('.') then fstr else fstr + ".0"
        | String string -> $"\"{string}\""
        | Array array -> this.ArrayToString(array, visited)
        | Object object -> this.ObjectToString(object, visited)
        | Tag tag -> this.TagToString(tag, visited)
        | Func func -> func.ToString()
        | Task _ -> "异步任务：……。"
        | Quote quote -> sprintf "元%A" quote

    member private this.TagToString(tag, visited) =
        let name = if isNull tag.Name then "？" else tag.Name

        let attrs =
            let visited = Set.add this visited

            seq { for pair in tag.Attrs -> $"{pair.Key}={stringOrEllipsis visited pair.Value}" }
            |> joinStrings " "

        let children =
            match tag.Children with
            | Nil -> ""
            | _ -> "，" + tag.Children.ToStringAux(visited)

        $"《{name} {attrs}{children}》"

    member private this.ObjectToString(object, visited) =
        let visited = Set.add this visited

        let items =
            seq {
                for key, value in object do
                    (key, stringOrEllipsis visited value) ||> sprintf "%s：%s"
            }

        let items =
            if isNull object.Wrapped then
                items
            else
                seq {
                    $"【{object.Wrapped}】"
                    yield! items
                }

        items |> joinStrings "，" |> sprintf "@｛%s｝"

    member private this.ArrayToString(array, visited) =
        let visited = Set.add this visited
        let items = Seq.map (stringOrEllipsis visited) array
        items |> joinStrings "，" |> sprintf "【%s】"

    override this.ToString() = this.ToStringAux(Set [ this ])

and Tag =
    { Name: string
      Attrs: Dictionary<string, QingValue>
      mutable Children: QingValue }

and IFunc =
    abstract Invoke: QingValue list * Map<VarOrFunc, QingValue> * Context -> QingValue
    abstract Bind: QingObject -> IFunc
    abstract ToString: unit -> string

and [<AllowNullLiteral>] Context(parent: Context option) =
    let vars = Dictionary<string, QingValue>()
    let funcs = Dictionary<string, IFunc>()

    new() = Context(None)
    new(parent: Context) = Context(Some parent)

    abstract GetVar: string -> QingValue option

    default this.GetVar(name) =
        match vars.TryGetValue(name) with
        | true, var -> Some var
        | false, _ ->
            match parent with
            | Some parent -> parent.GetVar(name)
            | None -> None

    member this.GetFunc(name) =
        match funcs.TryGetValue(name) with
        | true, func -> Some func
        | false, _ ->
            match parent with
            | Some parent -> parent.GetFunc(name)
            | _ -> None

    abstract SetVar: bool * string * QingValue -> unit

    default _.SetVar(recursive, name, value) =
        match parent, vars.ContainsKey(name), recursive with
        | Some parent, false, true -> parent.SetVar(true, name, value)
        | _ -> vars[name] <- value

    member this.SetVar(name, value) = this.SetVar(false, name, value)

    member _.SetFunc(recursive, name, value) =
        match parent, funcs.ContainsKey(name), recursive with
        | Some parent, false, true -> parent.SetFunc(true, name, value)
        | _ -> funcs[name] <- value

    member this.SetFunc(name, value) = this.SetFunc(false, name, value)

    member this.Set(recursive, target, value) =
        match target, value with
        | Var _, Func _ -> Error $"不可用函数赋值"
        | Var name, _ ->
            this.SetVar(recursive, name, value)
            Ok()
        | VarOrFunc.Func name, Func fn ->
            this.SetFunc(recursive, name, fn)
            Ok()
        | VarOrFunc.Func _, _ -> Error $"{value.GetTypeString()}不是函数"

    member _.Count = vars.Count + funcs.Count

    interface System.Collections.IEnumerable with
        member _.GetEnumerator() = raise <| NotImplementedException()

    interface IEnumerable<string * QingValue> with
        member _.GetEnumerator() : IEnumerator<string * QingValue> =
            (seq {
                for pair in vars -> "#" + pair.Key, pair.Value
                for pair in funcs -> "@" + pair.Key, Func pair.Value
            })
                .GetEnumerator()

and [<AbstractClass>] Property() =
    abstract Get: QingObject -> QingValue
    abstract Set: QingObject * QingValue -> unit

and [<AllowNullLiteral>] QingObject(prototype: Context option, properties: Map<string, Property>) =
    inherit Context(prototype)

    new() = QingObject(None, Map.empty)
    new(prototype: Context) = QingObject(Some prototype, Map.empty)

    new(entries) as this =
        QingObject()
        then
            for key, value in entries do
                this.SetVar(false, key, value)

    abstract Wrapped: obj
    default _.Wrapped = null

    override this.GetVar(name) =
        match Map.tryFind name properties with
        | Some prop -> Some <| prop.Get(this)
        | None -> base.GetVar(name)

    override this.SetVar(recursive, name, value) =
        match Map.tryFind name properties with
        | Some prop -> prop.Set(this, value)
        | None -> base.SetVar(recursive, name, value)
