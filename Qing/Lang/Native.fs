namespace Qing.Lang

module Native =
    let inline internal tryExec ([<InlineIfLambda>] thunk) =
        try
            thunk ()
        with
        | QingFuncException _ -> reraise ()
        | exn -> raise <| QingFuncException exn.Message

    let inline internal raiseNotBound name =
        raise <| QingFuncException $"方法@{name}未绑定到实例"

    let inline private tryExecFs name ([<InlineIfLambda>] thunk) =
        try
            thunk ()
        with
        | QingFuncException _ -> reraise ()
        | :? MatchFailureException -> raise <| QingFuncException $"{name}函数的参数错误"
        | exc -> raise <| QingFuncException exc.Message

    let create name desc impl =
        { new IFunc with
            member _.Invoke(args, _, context) =
                tryExecFs name (fun () -> impl args context)

            member this.Bind(_) = this
            member _.ToString() = $"@原生函数-{name}｛{desc}｝" }

    let rec private createBoundMethod impl self this name =
        { new IFunc with
            member _.Invoke(args, _, context) =
                tryExecFs name (fun () -> impl args self context)

            member this.Bind(self) = createBoundMethod impl self this name
            member _.ToString() = this.ToString() }

    let createMethod name desc impl =
        { new IFunc with
            member _.Invoke(args, _, context) =
                match args with
                | Object self :: args -> tryExecFs name (fun () -> impl args self context)
                | _ -> raiseNotBound name

            member this.Bind(self) = createBoundMethod impl self this name

            member _.ToString() = $"@绑定方法-{name}｛{desc}｝" }


[<AbstractClass>]
type Native() =
    abstract Name: string
    abstract Desc: string
    abstract Invoke: QingValue[] * Context -> QingValue

    abstract Bind: QingObject -> IFunc
    default this.Bind(_) = this

    interface IFunc with
        member this.Invoke(args, _, context) =
            Native.tryExec (fun () -> this.Invoke(Array.ofList args, context))

        member this.Bind(self) = this.Bind(self)

        member this.ToString() = $"@原生函数-{this.Name}｛{this.Desc}｝"

[<AbstractClass>]
type NativeMethod() =
    inherit Native()

    abstract InvokeMethod: QingValue[] * QingObject * Context -> QingValue

    override this.Invoke(args, context) =
        if Array.isEmpty args then
            Native.raiseNotBound this.Name

        match args[0] with
        | Object self -> Native.tryExec (fun () -> this.InvokeMethod(args[1..], self, context))
        | _ -> Native.raiseNotBound this.Name

    override this.Bind(self) = BoundMethod(this, self)

and BoundMethod(method: NativeMethod, self) =
    interface IFunc with
        member _.Invoke(args, _, context) =
            Native.tryExec (fun () -> method.InvokeMethod(Array.ofList args, self, context))

        member _.Bind(self) = BoundMethod(method, self)

        member _.ToString() = $"@绑定方法-{method.Name}｛{method.Desc}｝"
