// 用于Parser.fs的工具模块.

// 为什么不用成熟, 高效且易用的FParsec呢?
// 因为青语言"没有使用任何现有的组件，完全掌握语法实现的每一个细节".

module Qing.Lang.Parsec

open System.Text.RegularExpressions

type CharStream(filename: string, source: string) =
    let mutable pos = 0
    let mutable slice = source

    member _.Pos = pos
    member _.FileName = filename
    member _.Source = source

    member _.Skip(step) =
        slice <- slice[step..]
        pos <- pos + step

    member _.Peek() = source[pos]
    member _.Eof = pos >= source.Length

    member _.Goto(pos') =
        slice <- source[pos'..]
        pos <- pos'

    member _.MatchString(s: string) = source[pos .. pos + s.Length - 1] = s

    member _.MatchRegex(regex: Regex) =
        let m = regex.Match(slice)
        if m.Success then Some m else None

    member this.Satisfy(pred) = not this.Eof && pred source[pos]

type ParserResult<'a> = Result<'a, string>

type Parser<'a> = CharStream -> ParserResult<'a>

let fail _ = Error "未知"
let fails s _ = Error s

let preturn x (stream: CharStream) = Ok x

let bind f x (stream: CharStream) =
    match x stream with
    | Ok result -> f result <| stream
    | Error msg -> Error msg

let inline (>>=) x f stream = bind f x stream

let map f x (stream: CharStream) =
    match x stream with
    | Ok result -> Ok <| f result
    | Error msg -> Error msg

let inline (|>>) x f stream = map f x stream

let inline (>>%) x y stream = map (fun _ -> y) x stream

let pipe2 p1 p2 f (stream: CharStream) =
    match p1 stream with
    | Ok a ->
        match p2 stream with
        | Ok b -> Ok <| f a b
        | Error msg -> Error msg
    | Error msg -> Error msg

let inline (.>>.) p1 p2 (stream: CharStream) = pipe2 p1 p2 (fun x y -> (x, y)) stream

let inline (.>>) p1 p2 (stream: CharStream) = pipe2 p1 p2 (fun x _ -> x) stream

let (>>.) p1 p2 (stream: CharStream) = pipe2 p1 p2 (fun _ y -> y) stream

let (<?>) p msg (stream: CharStream) =
    match p stream with
    | Ok _ as ok -> ok
    | Error _ -> Error msg

let (<|>) p1 p2 (stream: CharStream) =
    let pos = stream.Pos

    match p1 stream with
    | Ok _ as ok -> ok
    | Error msg1 as error ->
        if stream.Pos = pos then
            match p2 stream with
            | Ok _ as ok -> ok
            | Error msg2 -> Error $"{msg1}或{msg2}"
        else
            error

let attempt p (stream: CharStream) =
    let pos = stream.Pos

    match p stream with
    | Ok _ as ok -> ok
    | Error _ as error ->
        stream.Goto(pos)
        error

let choice ps = Seq.reduce (<|>) ps

type ParserCombinator() =
    member _.Bind(x, f) = bind f x
    member _.Return(x) = preturn x
    member _.ReturnFrom(p) = p

let parse = ParserCombinator()

let eof (stream: CharStream) =
    if stream.Eof then Ok() else Error "文件结束"

let askpos (stream: CharStream) = Ok stream.Pos
let askfilename (stream: CharStream) = Ok stream.FileName
let asksource (stream: CharStream) = Ok stream.Source

let pstring s (stream: CharStream) =
    if stream.MatchString(s) then
        stream.Skip(s.Length)
        Ok s
    else
        Error $"\"{s}\""

let regex re =
    let msg = $"符合正则表达式\"{re}\""
    let re = Regex("^" + re)

    fun (stream: CharStream) ->
        match stream.MatchRegex(re) with
        | Some m ->
            stream.Skip(m.Length)
            Ok m
        | None -> Error msg

let regexs re = regex re |>> string

let group (n: int) (m: Match) = m.Groups[n].Value

let many p (stream: CharStream) =
    let rec loop acc =
        match attempt p stream with
        | Ok result -> loop <| result :: acc
        | Error _ -> preturn (List.rev acc) stream

    loop []

let notEmpty p (stream: CharStream) =
    let pos = stream.Pos

    match p stream with
    | Ok _ when stream.Pos = pos -> fail stream
    | Ok _ as ok -> ok
    | Error _ as error -> error

let opt p (stream: CharStream) =
    match attempt p stream with
    | Ok result -> Ok <| Some result
    | Error _ -> Ok None

let satisfy pred (stream: CharStream) =
    if stream.Satisfy(pred) then
        let c = stream.Peek()
        stream.Skip(1)
        Ok c
    else
        fail stream

let manyChars (p: Parser<char>) (stream: CharStream) =
    match many p stream with
    | Ok chars -> preturn (System.String.Join("", chars)) stream
    | Error msg -> Error msg

let noneOf chars =
    satisfy (fun c -> chars |> Seq.contains c |> not)

let between left right middle = left >>. middle .>> right

let createForwardReference () =
    let parserRef = ref (fun _ -> failwith "dummy parser")
    let parser stream = parserRef.Value stream
    parser, parserRef
