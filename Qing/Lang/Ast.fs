namespace Qing.Lang

type Operator =
    | Add
    | Sub
    | Mul
    | Div
    | Mod
    | Pow
    | Eq
    | Ne
    | Lt
    | Gt
    | Le
    | Ge
    | And
    | Or

    override this.ToString() =
        match this with
        | Add -> "加"
        | Sub -> "减"
        | Mul -> "乘"
        | Div -> "除"
        | Mod -> "取模"
        | Pow -> "乘方"
        | Eq -> "等于"
        | Ne -> "不等于"
        | Gt -> "大于"
        | Lt -> "小于"
        | Ge -> "大于等于"
        | Le -> "小于等于"
        | And -> "且"
        | Or -> "或"

type VarOrFunc =
    | Var of name: string
    | Func of name: string

    override this.ToString() =
        match this with
        | Var name -> "#" + name
        | Func name -> "@" + name

type AstData<'Value> =
    | Constant of value: 'Value
    | Not of value: Ast<'Value>
    | BinOp of left: Ast<'Value> * op: Operator * right: Ast<'Value>
    | Path of path: Path<'Value>
    | Array of elts: Ast<'Value> list
    | Object of entries: (VarOrFunc * Ast<'Value>) list
    | Tag of name: string * attrs: (string * Ast<'Value>) list * children: Ast<'Value> option
    | Lambda of
        async: bool *
        parameters: VarOrFunc list *
        defparams: (VarOrFunc * Ast<'Value>) list *
        desc: string option *
        body: Ast<'Value> list
    | Assign of recursive: bool * target: Path<'Value> * value: Ast<'Value>
    | AugAssign of target: Path<'Value> * op: Operator * value: Ast<'Value>
    | If of test: Ast<'Value> * body: Ast<'Value> list * orelse: Ast<'Value> list
    | For of init: Ast<'Value> * test: Ast<'Value> * step: Ast<'Value> * body: Ast<'Value> list
    | While of test: Ast<'Value> * body: Ast<'Value> list
    | Until of test: Ast<'Value> * body: Ast<'Value> list
    | Repeat of times: Ast<'Value> * body: Ast<'Value> list
    | ForEach of target: VarOrFunc * iter: Ast<'Value> * body: Ast<'Value> list
    | Try of body: Ast<'Value> list * handler: Handler<'Value> * finalbody: Ast<'Value> list
    | Throw of exn: Ast<'Value>
    | Return of value: Ast<'Value>
    | Break
    | Continue
    | Switch of subject: Ast<'Value> * cases: Case<'Value> list * def: Ast<'Value> list
    | Await of value: Ast<'Value>

and Handler<'Value> = Handler of target: string * body: Ast<'Value> list

and Case<'Value> = Case of value: Ast<'Value> * body: Ast<'Value> list

and Segment<'Value> =
    | VarSegment of name: string
    | FuncSegment of name: string
    | IntSegment of index: int
    | ExprSegment of expr: Ast<'Value>
    | CallSegment of posargs: Ast<'Value> list * kwargs: (VarOrFunc * Ast<'Value>) list

and Path<'Value> = Path of root: VarOrFunc * subsequence: Segment<'Value> list

and Ast<'Value> =
    { Data: AstData<'Value>
      Pos: int
      FileName: string
      Source: string }
