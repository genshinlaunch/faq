module Qing.Tests.Parser

open FsUnit.Xunit
open FsUnit.CustomMatchers
open Xunit

open Qing.Lang
open Parsec
open Parser.Parsers

let rec eraseSourceInfo ast =
    let data =
        match ast.Data with
        | Not value -> value |> eraseSourceInfo |> Not
        | BinOp(left, op, right) -> BinOp(eraseSourceInfo left, op, eraseSourceInfo right)
        | AstData.Path path -> path |> erasePathSourceInfo |> AstData.Path
        | Array elts -> elts |> List.map eraseSourceInfo |> Array
        | Object entries -> entries |> eraseValues |> Object
        | Tag(name, attrs, children) -> Tag(name, eraseValues' attrs, Option.map eraseSourceInfo children)
        | Lambda(async, parameters, defparams, desc, body) ->
            Lambda(async, parameters, eraseValues defparams, desc, List.map eraseSourceInfo body)
        | Assign(recursive, target, value) -> Assign(recursive, erasePathSourceInfo target, eraseSourceInfo value)
        | AugAssign(target, op, value) -> AugAssign(erasePathSourceInfo target, op, eraseSourceInfo value)
        | Await value -> value |> eraseSourceInfo |> Await
        | For(init, test, step, body) ->
            For(eraseSourceInfo init, eraseSourceInfo test, eraseSourceInfo step, List.map eraseSourceInfo body)
        | ForEach(target, iter, body) -> ForEach(target, eraseSourceInfo iter, List.map eraseSourceInfo body)
        | If(test, body, orelse) ->
            If(eraseSourceInfo test, List.map eraseSourceInfo body, List.map eraseSourceInfo orelse)
        | Repeat(times, body) -> Repeat(eraseSourceInfo times, List.map eraseSourceInfo body)
        | Return value -> value |> eraseSourceInfo |> Return
        | Switch(subject, cases, def) ->
            let mapCase (Case(value, body)) =
                Case(eraseSourceInfo value, List.map eraseSourceInfo body)

            Switch(eraseSourceInfo subject, List.map mapCase cases, List.map eraseSourceInfo def)
        | Throw exc -> exc |> eraseSourceInfo |> Throw
        | Try(body, Handler(target, handlerbody), finalbody) ->
            Try(
                List.map eraseSourceInfo body,
                Handler(target, List.map eraseSourceInfo handlerbody),
                List.map eraseSourceInfo finalbody
            )
        | Until(test, body) -> Until(eraseSourceInfo test, List.map eraseSourceInfo body)
        | While(test, body) -> While(eraseSourceInfo test, List.map eraseSourceInfo body)
        | Constant(Quote ast) -> ast |> List.map eraseSourceInfo |> Quote |> Constant
        | Break
        | Continue
        | Constant _ -> ast.Data

    { Data = data
      Source = ""
      FileName = ""
      Pos = 0 }

and erasePathSourceInfo (Path(root, segments)) =
    let mapping =
        function
        | ExprSegment expr -> expr |> eraseSourceInfo |> ExprSegment
        | CallSegment(posargs, kwargs) -> CallSegment(List.map eraseSourceInfo posargs, eraseValues kwargs)
        | seg -> seg

    Path(root, List.map mapping segments)

and eraseValues entries =
    let mapping (key, value) = key, eraseSourceInfo value
    List.map mapping entries

// 不然会"This construct causes code to be less generic than indicated by the type annotations."
and eraseValues' entries =
    let mapping (key, value) = key, eraseSourceInfo value
    List.map mapping entries

// 用来方便地由AstData创建Ast.
// Fantomas(6.2.1)会以为它是二元+运算符, 因此不要用fantomas格式化这个文件.
let (~+) data =
    { Data = data
      Pos = 0
      FileName = ""
      Source = "" }

type Ast = Ast<QingValue>

let parsedFrom (parser: Parser<Ast>) source (target: Ast) =
    (parser .>> eof) (CharStream("测试", source))
    |> Result.map eraseSourceInfo
    |> should equal (ParserResult<Ast>.Ok target)
// Result的类型参数是必须的, 因为xunit用弱类型的Equals方法判断是否相等.

let parsedFrom' parser = parsedFrom (parser |>> (~+))

let failsOn source (parser: Parser<'a>) =
    (parser .>> eof) (CharStream("测试", source))
    |> should be (ofCase <@ ParserResult<'a>.Error @>)

let pathParsedFrom source target =
    (ppath .>> eof) (CharStream("测试", source))
    |> Result.map erasePathSourceInfo
    |> should equal (ParserResult<Path<QingValue>>.Ok target)

[<Fact>]
let ``常量`` () =
    + Constant(Int 1)
    |> parsedFrom' pconstant "1"
    +Constant(Decimal 1.5M)
    |> parsedFrom' pconstant "1.5"
    +Constant(Binary [| 1uy |])
    |> parsedFrom' pconstant "0X1"
    +Constant(Bool true)
    |> parsedFrom' pconstant "真"
    +Constant Nil
    |> parsedFrom' pconstant "空"
    +Constant(Quote [ +Constant(Int 1) ])
    |> parsedFrom' pconstant "｛1｝"
    +Constant(String "aaa")
    |> parsedFrom' pconstant "\"aaa\""

[<Fact>]
let ``路径`` () =
    Path(Var "x", []) |> pathParsedFrom "#x"
    Path(Func "f", []) |> pathParsedFrom "@f"
    ppath
    |> failsOn "@f[#x=1 #x=2]"
    Path(Func "f", [ CallSegment([ +Constant(Int 1) ], [ Var "x", +Constant(Int 2) ]) ])
    |> pathParsedFrom "@f[#x=1]"
    Path(Var "a", [ VarSegment "b"; ExprSegment(+Constant(Int 1)); IntSegment 0 ])
    |> pathParsedFrom "#a#b#[1]#0"

// [<Fact>]
// let ``赋值`` () =
//     Assign(Var "x")

// [<Fact>]
let ``取什么名字好呢`` () =
    +Constant(Int 1)
    |> parsedFrom past "1"
    +Constant(Int 2)
    |> parsedFrom pparen "(2)"
    +Constant(Int 3)
    |> parsedFrom patom "(3)"
    +Constant(Int 4)
    |> parsedFrom past "(3)"
